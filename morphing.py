import csv

import numpy as np
import PIL.Image
import pandas as pd

from helpers import get_data
from warping import warp_image


def add_boundary(segs, h, w):
    pb = np.array(([1, 1], [1, h], [w, h] ,[w, 1]))
    qb = np.array(([1, h] ,[w, h], [w, 1], [1, 1]))
    segs_b = construct_segs(pb, qb)
    segs = segs + segs_b
    return segs


def load_face_model(landmarks):
    con = [[float(i), float(i+1)] for i in range(65)]

    model = list(landmarks)

    _segs = []
    for i in range(len(con)):
        p = model[int(con[i][0])]
        q = model[int(con[i][1])]
        _segs.append({})
        _segs[i]['p'] = {}
        _segs[i]['p']['x'] = float(p[0])
        _segs[i]['p']['y'] = float(p[1])
        _segs[i]['q'] = {}
        _segs[i]['q']['x'] = float(q[0])
        _segs[i]['q']['y'] = float(q[1])

    return _segs


def construct_segs(pp, qq):
    segs_ = []
    for i in range(pp.shape[0]):
        p = {}
        p['x'] = pp[i][0]
        p['y'] = pp[i][1]
        q = {}
        q['x'] = qq[i][0]
        q['y'] = qq[i][1]
        segs_.append({})
        segs_[i]['p'] = p
        segs_[i]['q'] = q
    return segs_


def get_weight(x, y, line):
    a = 10; b = 1; p = 1; d = 1
    [u, v] = get_uv(x, y, line)

    d1 = ((x - line['q']['x'])**2 + (y - line['q']['y'])**2)**0.5
    d2 = ((x - line['p']['x'])**2 + (y - line['p']['y'])**2)**0.5
    d =  np.abs(v)

    d[u > 1] = d1[u > 1]
    d[u < 0] = d2[u < 0]

    pq = {}
    pq['x'] = line['q']['x']-line['p']['x']
    pq['y'] = line['q']['y']-line['p']['y']
    lenn = ((pq['x'])**2 + (pq['y'])**2)**0.5
    w = (lenn**p/(a+d))**b
    return w


def get_uv(x, y, line):
    p = line['p']
    q = line['q']
    pq = {}
    pq['x'] = q['x']-p['x']
    pq['y'] = q['y']-p['y']
    len_2 = pq['x']**2 + pq['y']**2
    lenn = len_2**0.5

    #B/A = (A'\B')' = matrix right division operator in MATLAB, which calls the MRDIVIDE function
    u = ((x-p['x'])*pq['x'] + (y-p['y'])*pq['y']) / len_2

    perp_pq = {}
    perp_pq['x'] = -pq['y']
    perp_pq['y'] = pq['x']
    v = ((x - p['x'])*perp_pq['x'] + (y - p['y'])*perp_pq['y']) / lenn
    return [u,v]


def get_xy(u, v, line):
    p = line['p']
    q = line['q']
    pq = {}
    pq['x'] = q['x'] - p['x']
    pq['y'] = q['y'] - p['y']
    perp_pq = {}
    perp_pq['x'] = -pq['y']
    perp_pq['y'] = pq['x']
    lenn = (pq['x']**2 + pq['y']**2)**0.5

    x = p['x'] + u*(q['x'] - p['x']) + (v * perp_pq['x']) / lenn
    y = p['y'] + u*(q['y'] - p['y']) + (v * perp_pq['y']) / lenn
    return [x, y]


def morph(im, style, img_landmarks, style_landmarks):
    #im = checkerboard(100,2,3)
    [h, w, nc] = style.shape

    x = np.arange(1, w+1)
    y = np.arange(1, h+1)
    [xx, yy] = np.meshgrid(x, y)

    segs_src = add_boundary(load_face_model(style_landmarks), h, w)
    segs_dst = add_boundary(load_face_model(img_landmarks), h, w)

    xsum = np.zeros(xx.shape)
    ysum = np.zeros(yy.shape)
    wsum = np.zeros(xx.shape)

    for i in range(len(segs_src)):
        [u, v] = get_uv(xx, yy, segs_src[i])
        [x, y] = get_xy(u, v, segs_dst[i])
        weights = get_weight(xx,yy, segs_src[i])
        wsum = wsum + weights
        xsum = xsum + weights*x
        ysum = ysum + weights*y

    x_m = np.divide(xsum, wsum)
    y_m = np.divide(ysum, wsum)
    vx = np.subtract(xx, x_m)
    vy = np.subtract(yy, y_m)

    x_m[np.isnan(x_m)] = 0
    y_m[np.isnan(y_m)] = 0

    vx[x_m < 1] = 0
    vx[x_m >= w] = 0
    vy[y_m < 1] = 0
    vy[y_m >= h] = 0

    return [vx, vy]


def main():
    img, img_landmarks = get_data('data/content')
    style, style_landmarks = get_data('data/style')
    img = np.asarray(img)
    style = np.asarray(style)
    morphed = morph(img, style, img_landmarks, style_landmarks)

    warped, mask = warp_image(style, morphed[0], morphed[1])

    warped = warped*255.
    warped = warped.astype('uint8')
    img = PIL.Image.fromarray(warped)
    img.save('data/output/warped.png')


if __name__ == '__main__':
    main()
