from collections import OrderedDict
import math

import PIL.Image
import dlib
import cv2
import numpy as np
import numpy.matlib

from helpers import get_data


FACIAL_LANDMARKS_IDXS = OrderedDict([
	("mouth", (48, 68)),
	("right_eyebrow", (17, 22)),
	("left_eyebrow", (22, 27)),
	("right_eye", (36, 42)),
	("left_eye", (42, 48)),
	("nose", (27, 36)),
	("jaw", (0, 17))
])


def rect_to_bb(rect):
	# take a bounding predicted by dlib and convert it
	# to the format (x, y, w, h) as we would normally do
	# with OpenCV
	x = rect.left()
	y = rect.top()
	w = rect.right() - x
	h = rect.bottom() - y

	# return a tuple of (x, y, w, h)
	return (x, y, w, h)

def shape_to_np(shape, dtype="int"):
	# initialize the list of (x, y)-coordinates
	coords = np.zeros((68, 2), dtype=dtype)

	# loop over the 68 facial landmarks and convert them
	# to a 2-tuple of (x, y)-coordinates
	for i in range(0, 68):
		coords[i] = (shape.part(i).x, shape.part(i).y)

	# return the list of (x, y)-coordinates
	return coords

def face_detection(image):
	detector = dlib.get_frontal_face_detector()
	predictor = dlib.shape_predictor('binaries/shape_predictor_68_face_landmarks.dat')
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

	# detect faces in the grayscale image
	rects = detector(gray, 1)

	# loop over the face detections
	for (i, rect) in enumerate(rects):
		# determine the facial landmarks for the face region, then
		# convert the facial landmark (x, y)-coordinates to a NumPy
		# array
		shape = predictor(gray, rect)
		shape = shape_to_np(shape)

	return shape


def visualize_facial_landmarks(image, shape, colors=None, alpha=0.75):
	# create two copies of the input image -- one for the
	# overlay and one for the final output image
	overlay = image.copy()
	output = image.copy()

	# if the colors list is None, initialize it with a unique
	# color for each facial landmark region
	if colors is None:
		colors = [(19, 199, 109), (79, 76, 240), (230, 159, 23),
			(168, 100, 168), (158, 163, 32),
			(163, 38, 32), (180, 42, 220)]

	# loop over the facial landmark regions individually
	for (i, name) in enumerate(FACIAL_LANDMARKS_IDXS.keys()):
		# grab the (x, y)-coordinates associated with the
		# face landmark
		(j, k) = FACIAL_LANDMARKS_IDXS[name]
		pts = shape[j:k]

		# check if are supposed to draw the jawline
		if name == "jaw":
			# since the jawline is a non-enclosed facial region,
			# just draw lines between the (x, y)-coordinates
			for l in range(1, len(pts)):
				ptA = tuple(pts[l - 1])
				ptB = tuple(pts[l])
				cv2.line(overlay, ptA, ptB, colors[i], 2)

		# otherwise, compute the convex hull of the facial
		# landmark coordinates points and display it
		else:
			hull = cv2.convexHull(pts)
			cv2.drawContours(overlay, [hull], -1, colors[i], -1)

	# apply the transparent overlay
	cv2.addWeighted(overlay, alpha, output, 1 - alpha, 0, output)
	return output


def rotate_pt(pt_in, angle, dims):
	sz_xy = np.matlib.repmat(np.array(dims)/2, 1, 1)
	angle = math.radians(angle)
	rot_mat = np.array([[math.cos(angle), math.sin(angle)],[-math.sin(angle), math.cos(angle)]])
	pt_out = (pt_in-sz_xy).dot(rot_mat) + sz_xy
	return pt_out


def crop_image(img, landmarks):
	# PAD IMAGE AND LMS
	img = PIL.Image.fromarray(img)
	pad = 1000
	landmarks = np.array(landmarks) + pad
	old_size = img.size
	new_size = (old_size[0] + 2*pad, old_size[1] + 2*pad)

	new_im = PIL.Image.new("RGB", new_size, color=(65,65,65,0))
	new_im.paste(img, (int((new_size[0] - old_size[0])/2),
	                   int((new_size[1] - old_size[1])/2)))

	# CALCULATE ROTATIONAL ANGLE BASED ON EYES
	fixed_eye_dist = 300
	left_eye = landmarks[36:42,:].mean(axis=0) # deduct 1 from points due to matlab indexing from 1, and matlab incl endpoints
	right_eye = landmarks[42:48,:].mean(axis=0)
	diff_eye = right_eye - left_eye
	eye_dist = sum(diff_eye**2)**0.5
	scale = fixed_eye_dist/eye_dist
	angle = math.atan(diff_eye[1]/diff_eye[0])/math.pi*180

	# RESIZE
	img = new_im.copy()
	new_size = (int(img.size[0]*scale) + 1, int(img.size[1]*scale) + 1)
	resized = img.resize(new_size)

	# RECALCULATE EYE CENTER
	dims = [resized.size[0], resized.size[1]]

	# ROTATE
	rotated = resized.rotate(angle, resample=PIL.Image.BILINEAR)
	left_eye_ = rotate_pt(scale*left_eye, -angle, dims)
	right_eye_ = rotate_pt(scale*right_eye, -angle, dims)
	center_ = (left_eye_ + right_eye_)/2
	center_ = center_.astype('uint32')[0]

	# CROP IMAGE
	width = 1000
	ld = 720
	ud = 600
	cropped = rotated.crop((center_[0] - int(width/2), center_[1] - ud,
	                        center_[0] + int(width/2), center_[1] + ld))

	# RECALCULATE LANDMARKS
	offset = center_
	offset[0] = center_[0] - width/2 - 1
	offset[1] = center_[1] - ud - 1

	landmarks = np.subtract(rotate_pt(scale*landmarks, -angle, dims),
	                        np.matlib.repmat(offset, landmarks.shape[0], 1))
	cropped = cropped.resize(old_size)
	cropped = np.asarray(cropped)
	return cropped, landmarks


def test_landmarks():
	# https://www.pyimagesearch.com/2017/04/03/facial-landmarks-dlib-opencv-python/
	image = PIL.Image.open('data/content.png')
	image = np.asarray(image)
	shape = face_detection(image)
	output = visualize_facial_landmarks(image, shape)
	im = PIL.Image.fromarray(output)
	im.save('data/output/with_landmarks.png')

	cropped_img, cropped_landmarks = crop_image(image, shape)
	cropped_img = cropped_img.astype('uint8')
	img = PIL.Image.fromarray(cropped_img)
	img.save('cropped.png')

def test_cropping():
	img, landmarks = get_data('data/content')
	landmarks = np.array(landmarks)
	img = np.asarray(img)
	cropped_img, cropped_landmarks = crop_image(img, landmarks)
	cropped_img = cropped_img.astype('uint8')
	img = PIL.Image.fromarray(cropped_img)
	img.save('cropped.png')


def main():
	test_landmarks()


if __name__ == '__main__':
    main()
