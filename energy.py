import numpy as np
import scipy.ndimage


def matlab_style_gauss2D(shape=(3,3),sigma=0.5):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma])
    """
    m,n = [(ss-1.)/2. for ss in shape]
    y,x = np.ogrid[-m:m+1,-n:n+1]
    h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
    h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h




def compute_energy(stack):
    result = []
    for i, s in enumerate(stack):
        energy = scipy.ndimage.gaussian_filter((s ** 2.0), sigma=1.0**(i+2))
        result.append(energy)
    return result
