import PIL.Image
import csv


def get_data(name):
    img = PIL.Image.open('%s.png' % name)
    landmarks = []
    with open('%s.lm' % name) as csvfile:
        landmarks = csv.reader(csvfile)
        landmarks = list(landmarks)
        landmarks = [[float(mark[0]), float(mark[1])] for mark in landmarks]
    return img, landmarks
