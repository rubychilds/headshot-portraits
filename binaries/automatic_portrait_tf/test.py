import sys

import numpy as np
import scipy.misc
import tensorflow as tf
import PIL.Image

from binaries.automatic_portrait_tf.net import FCN8s

fcn = FCN8s(2)

COLOR_SET = [[0, 0, 0], [255, 255, 255]]


def build_image(filename):
    MEAN_VALUES = np.array([104.00698793, 116.66876762, 122.67891434])
    MEAN_VALUES = MEAN_VALUES.reshape((1, 1, 1, 3))
    img = scipy.misc.imread(filename, mode='RGB')[:, :, ::-1]
    height, width, _ = img.shape
    img = np.reshape(img, (1, height, width, 3)) - MEAN_VALUES
    return img


def save_image(image, filename):
    scipy.misc.imsave(filename, image)


def test(image_name, net=fcn.net):
    image = build_image(image_name)

    with tf.Session() as sess:
        saver = tf.train.Saver(tf.global_variables())
        model_file = tf.train.latest_checkpoint('binaries/automatic_portrait_tf/model/')
        if model_file:
            saver.restore(sess, model_file)
        else:
            raise Exception('Testing needs pre-trained model!')

        feed_dict = {
            net['image']: image,
            net['drop_rate']: 1
        }
        result = sess.run(tf.argmax(net['score'], dimension=3),
                          feed_dict=feed_dict)
    return result


def create_matt(image_location='image.png', output_location='result.png', save=False):
    result= test(image_location)
    s = set()
    _, h, w = result.shape
    result = result.reshape(h*w)
    image = []
    for v in result:
        image.append(COLOR_SET[v])
        if v not in s:
            s.add(v)
    image = np.array(image)
    image = np.reshape(image, (h, w, 3))
    if save:
        save_image(image, output_location)
    return np.uint8(image)


if __name__ == '__main__':
    create_matt(sys.argv[1], sys.argv[2], save=True)
