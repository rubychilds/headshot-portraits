import math

import PIL.Image
import skimage.color
import numpy as np


def load_image(img_name):
    return PIL.Image.open(img_name)

def img2array(img):
    return np.asarray(img)

def array2img(array):
    array = array.astype('uint8')
    return PIL.Image.fromarray(array)

def rgb2lab(rgb):
    return skimage.color.rgb2lab(rgb)

def lab2rgb(lab):
    a_min = np.array([0.0, -50.0, -50.0], dtype=np.float32).reshape((1,1,3))
    a_max = np.array([100.0, +50.0, +50.0], dtype=np.float32).reshape((1,1,3))
    return skimage.color.lab2rgb(lab.clip(a_min, a_max)) * 255.0

def match_image_sizes(img1, img2):
    w1, h1 = img1.size
    w2, h2 = img2.size
    if w1 < w2:
        img2 = img2.resize((w1, h1))

    return img1, img2

def main():
    img = load_image('data/content.png')
    array = img2array(img)
    lab = rgb2lab(array)
    array = lab2rgb(lab)
    img = array2img(array)
    img.save('data/output/after_rgb_lab.png')


if __name__ == '__main__':
    main()
