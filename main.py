from image import rgb2lab, lab2rgb, match_image_sizes
from morphing import morph
from warping import warp_image
from pyramids import construct_pyramids, reconstruct_pyramids
from gain import transfer_gain, compute_gain
from energy import compute_energy
from landmarks import face_detection, crop_image
from matting import get_matt, combine_matt, apply_matt_stack
from flo import calculate_flo
from thresh_v import thresh_v

import numpy as np
import PIL.Image
import skimage


def style_transfer():

    print('in style', np.__path__, np.__version__, np.matlib)

    im_in = 'data/style.png'
    style_in = 'data/content.png'
    background = 'data/0.png'

    im_in = PIL.Image.open(im_in)
    style_in = PIL.Image.open(style_in)
    background = PIL.Image.open(background).convert('RGB')

    im_in.thumbnail((500,500))
    style_in.thumbnail((500,500))
    background.thumbnail((500,500))

    # ensure inputs are same size
    im_in, style_in = match_image_sizes(im_in, style_in)
    background = background.resize(im_in.size)

    im_in = np.asarray(im_in)
    style_in = np.asarray(style_in)
    background = np.asarray(background)

    # load landmarks
    landmarks_in = face_detection(im_in)
    landmarks_style = face_detection(style_in)

    # crop and align faces
    im_in, landmarks_in = crop_image(im_in, landmarks_in)
    im_style, landmarks_style = crop_image(style_in, landmarks_style)

    style_in2 = style_in.copy()
    style_in2.resize(im_in.shape)

    print('MATTS')
    # Load masks
    mask_in = get_matt(im_in)
    mask_style = get_matt(style_in2)

    print('MORPH')
    [vxm, vym] = morph(im_in, style_in2, landmarks_in, landmarks_style)

    print('WARP')
    style_in_w, _ = warp_image(style_in2, vxm, vym)
    [vx, vy] = calculate_flo(im_in, style_in_w)

    print('THRESH')
    [vxf, vyf] = thresh_v(vx + vxm, vy + vym)

    im_in = mask_in*im_in + np.logical_not(mask_in)*background

    print('PYRAMIDS')
    im_in = im_in.astype('uint8')
    style_in_w = style_in_w.astype('uint8')

    im_stack = construct_pyramids(im_in, mask_in)
    style_stack = construct_pyramids(style_in_w, combine_matt(mask_in, mask_style))

    style_stack = [warp_image(e, vxf, vyf)[0] for e in style_stack]

    im_energy = compute_energy(im_stack)
    style_energy = compute_energy(style_stack)

    gain = [compute_gain(img, sty) for img, sty in zip(im_energy, style_energy)]
    im_stack = transfer_gain(im_stack, gain)

    img = reconstruct_pyramids(im_stack)

    img = mask_in*img + np.logical_not(mask_in)*background

    img = img.astype('uint8')
    img = PIL.Image.fromarray(img)
    img.save('main_output.png')

def main():
    style_transfer()

if __name__ == '__main__':
    main()
