from warping import warp_image
import numpy as np


def thresh_v(vx, vy):
    [h, w] = vx.shape
    [_, mask] = warp_image(np.zeros((h, w, 3)), vx, vy)
    mask = np.ones((h,w)) - mask
    mask = mask.astype('bool')
    # vx[1-mask], vy[1-mask] = 0, 0
    vx[mask], vy[mask] = 0, 0
    return [vx, vy]
