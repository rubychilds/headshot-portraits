import numpy as np


def transfer_gain(stack, gain):
    return [(p * g) for p, g in zip(stack, gain)]

def compute_gain(energy1, energy2, min_gain=0.9, max_gain=2.8):
    g = np.sqrt(np.abs(energy2 / (energy1 + 1E-4)))
    g = np.clip(g, min_gain, max_gain)
    return g
