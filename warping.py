import numpy as np
import scipy
import scipy.interpolate


def warp_image(im, vx, vy, mode='offset'):
    height2, width2, nchannels = im.shape
    height1, width1 = vx.shape

    x = np.arange(1, width2+1)
    y = np.arange(1, height2+1)
    xx, yy = np.meshgrid(x, y)
    x = np.arange(1, width1+1)
    y = np.arange(1, height1+1)
    XX, YY = np.meshgrid(x, y)

    if mode == 'offset':
        XX = np.add(XX, vx)
        YY = np.add(YY, vy)

    elif mode == 'abs':
        XX = vx
        YY = vy

    masks = [XX < 1, XX > width2, YY < 1, YY > height2]

    mask = np.ma.mask_or(masks[0], masks[1], shrink=False)
    mask = np.ma.mask_or(masks[2], mask, shrink=False)
    mask = np.ma.mask_or(masks[3], mask, shrink=False)
    XX = np.minimum(np.maximum(XX, 0), width2-1)
    YY = np.minimum(np.maximum(YY, 0), height2-1)

    warpI2 = np.empty((height2, width2, nchannels))
    for i in range(0, nchannels):
        foo = image_interp(im[:,:,i], XX, YY)
        foo /= 255.
        foo[mask] = 0.6
        warpI2[:,:,i] = foo * 255.

    warpI2 = np.clip(warpI2, 0., 255.)
    mask = 1 - mask

    return warpI2.astype('uint8'), mask


def image_interp(im, XX, YY):
    """Interpolate values in im at xy[:,0],xy[:,1]."""
    f = scipy.interpolate.RectBivariateSpline(np.arange(0,im.shape[1],1),np.arange(0,im.shape[0],1),im.T)
    return f(XX, YY, grid=False)
