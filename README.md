### Style Transfer for Headshot Portraits

### Paper

Based on the paper 'Style Transfer for Headshot Portraits' https://people.csail.mit.edu/yichangshih/portrait_web/

### Requirements

* Ubuntu 16.04
* Python 3.6
* OpenCV 3: install with Python to use as cv2 https://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/
* DLib 19.8.1 https://www.pyimagesearch.com/2017/03/27/how-to-install-dlib/
* Tensorflow 1.4
* PyCaffe 1.0 built from https://github.com/BVLC/caffe (does require CUDA and NVidia GPU)
* Pre-built binaries using deepflow and deepmatching

### Binaries + Other

* Deep Flow 2 https://thoth.inrialpes.fr/src/deepflow/
* Deep Matching https://thoth.inrialpes.fr/src/deepmatching/
* Auto Portrait Matting in Tensorflow https://github.com/Corea/automatic-portrait-tf
* KNN-Matting https://github.com/MarcoForte/knn-matting

*** Thanks to PyImage search for some of the tutorials in dealing with installation, as well as other OS contributors ***
