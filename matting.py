import os
import warnings

import PIL.Image
import numpy as np
import sklearn.neighbors
import scipy.sparse
import cv2
import skimage.morphology

from binaries.automatic_portrait_tf.test import create_matt


def knn_matte(img, trimap, mylambda=100):
    [m, n, c] = img.shape
    img, trimap = img/255, trimap/255
    foreground = (trimap > 0.99).astype(int)
    background = (trimap < 0.01).astype(int)
    all_constraints = foreground + background

    a, b = np.unravel_index(np.arange(m*n), (m, n))
    feature_vec = np.append(np.transpose(img.reshape(m*n,c)), [ a, b]/np.sqrt(m*m + n*n), axis=0).T
    nbrs = sklearn.neighbors.NearestNeighbors(n_neighbors=10, n_jobs=4).fit(feature_vec)
    knns = nbrs.kneighbors(feature_vec)[1]

    # Compute Sparse A
    row_inds = np.repeat(np.arange(m*n), 10)
    col_inds = knns.reshape(m*n*10)
    vals = 1 - np.linalg.norm(feature_vec[row_inds] - feature_vec[col_inds], axis=1)/(c+2)
    A = scipy.sparse.coo_matrix((vals, (row_inds, col_inds)),shape=(m*n, m*n))

    D_script = scipy.sparse.diags(np.ravel(A.sum(axis=1)))
    L = D_script-A
    D = scipy.sparse.diags(np.ravel(all_constraints[:,:, 0]))
    v = np.ravel(foreground[:,:,0])
    c = 2*mylambda*np.transpose(v)
    H = 2*(L + mylambda*D)

    warnings.filterwarnings('error')
    alpha = []
    try:
        alpha = np.minimum(np.maximum(scipy.sparse.linalg.spsolve(H, c), 0), 1).reshape(m, n)
    except Warning:
        x = scipy.sparse.linalg.lsqr(H, c)
        alpha = np.minimum(np.maximum(x[0], 0), 1).reshape(m, n)
    warnings.filterwarnings('ignore')
    return alpha


def compute_matt(img, trimap):
    img = np.asarray(img)# [:,:,:3]
    # trimap = np.asarray(trimap)[:,:,:3]
    alpha = knn_matte(img, trimap)
    alpha = alpha * 255
    return alpha


def combine_matt(matt1, matt2):
    return np.logical_or(matt1, matt2)


def get_matt(image):
    image = PIL.Image.fromarray(image)
    image_name = 'temp.png'
    image.save(image_name)
    initial_matt = create_matt(image_name)
    os.remove(image_name)
    arr = np.asarray(initial_matt)
    edges = cv2.Canny(arr,100,200)

    for i in range(30):
        edges = skimage.morphology.dilation(edges)
    edges = np.repeat(edges[:,:,np.newaxis], 3, axis=2)
    arr[edges == 255] = 125
    matt = compute_matt(image, arr)
    matt[matt > 125] = 255
    matt[matt <= 125] = 0
    return np.repeat(matt[:,:,np.newaxis], 3, axis=2)/255.


def apply_matt_stack(stack, matt):
    return [s*matt for s in stack]


def main():
    image = PIL.Image.open('data/style.png')
    image.thumbnail((300,300))
    matt = get_matt(np.asarray(image))
    PIL.Image.fromarray(np.uint8(matt)).save('data/output/matt.png')


if __name__ == '__main__':
    main()
