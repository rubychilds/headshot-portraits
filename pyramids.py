import numpy as np, sys
import PIL.Image
from scipy import ndimage
import skimage.color


def save_image(image, i):
    a_min = np.array([0.0, -50.0, -50.0], dtype=np.float32).reshape((1,1,3))
    a_max = np.array([100.0, +50.0, +50.0], dtype=np.float32).reshape((1,1,3))
    image = np.uint8(skimage.color.lab2rgb(image.clip(a_min, a_max)) * 255.0)
    PIL.Image.fromarray(image).save('pyramid_%d.png' %i)

def reconstruct_pyramids(stack):
    img = sum(stack)
    a_min = np.array([0.0, -50.0, -50.0], dtype=np.float32).reshape((1,1,3))
    a_max = np.array([100.0, +50.0, +50.0], dtype=np.float32).reshape((1,1,3))
    return np.uint8(skimage.color.lab2rgb(img.clip(a_min, a_max)) * 255.0)


def gaussian_blur(img, sigma):
    """Version of scipy's gaussian_filter that only considers X and Y, not color-channel filtering.
    """
    img = ndimage.gaussian_filter1d(img, sigma=sigma, axis=0)
    return ndimage.gaussian_filter1d(img, sigma=sigma, axis=1)


def construct_pyramids(img, matte=np.ones((1,1))):
    if matte.shape == (1,1):
        matte = np.ones(img.shape)
    img = skimage.color.rgb2lab(img)
    blurred = [img]
    for i in range(1, 6):
        sigma = 2**i
        blurred += [gaussian_blur(img*matte, sigma)]
    for i in range(0, 5):
        blurred[i] = (blurred[i] - blurred[i+1])*matte
    blurred[5] = blurred[5]
    return blurred

def main():
    img = PIL.Image.open('data/content.png')
    img = np.asarray(img)
    stack = construct_pyramids(img)
    array = reconstruct_pyramids(stack)
    img = PIL.Image.fromarray(array)
    img.save('pyramid_output.png')

if __name__ == '__main__':
    main()
